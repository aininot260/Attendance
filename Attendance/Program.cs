﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBOPlib;  //这个是自己写的和数据库操作的库，这里引用进来
using DotNet.Utilities;  //这是自己写的和网络通信有关的库，这个和上面那个在解决方案里面都可以找到
using System.Net.Sockets; //用到和网络通信有关的命名空间
using System.Net;  //同上
using System.Threading;  //用到了多线程
using System.IO;
using QCloud.CosApi.Common;  //这个是引用的外部加进来的对象存储和人脸对比的库，详情就是Common文件夹下的内容
namespace Attendance
{
    class Program
    {
        public static byte[] buff;  //这个就是两边传输的时候传输的东西，就是加工之后的数据
        public static string str;  //这个是加工之前的数据
        public static Socket socket;  //这里建立一个socket对象用于在服务器端侦听，看有没有人来连接服务器
        public static DBOP Query = new DBOP();  //将和数据库操作有关的类实例化，详情请看项目DBOPlib
        const int APP_ID = 1252885834;  //以下的几条都是和对象存储和人脸对比相关的配置信息
        const string SECRET_ID = "AKIDfFgyABujEXecT8bt1xqX4QXTDl9LnWaT";
        const string SECRET_KEY = "qJhtfLsFWcZekfO1Shk7Og1HMFwRktwo";
        const string bucketName = "attendance";
        static void Main(string[] args)
        {
            //SERVER
            IPAddress ip = IPAddress.Parse("0.0.0.0");
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(new IPEndPoint(ip, 10010));
            socket.Listen(10);
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("签到系统服务器端启动成功，监听{0}", socket.LocalEndPoint.ToString());
            Thread listenthread = new Thread(listen);
            listenthread.Start();

        }
        private static void listen()  //线程函数用于接收每一个用户
        {
            while (true)
            {
                Socket clientsocket = socket.Accept();
                Thread workthread = new Thread(work);
                workthread.Start(clientsocket);
            }
        }
        private static void work(object clientsocket)  //线程函数用于处理每一个用户
        {
            Socket mysocket = (Socket)clientsocket; //对于每一个接收到的用户
            try
            {
                buff = new byte[65536];
                int cnt = mysocket.Receive(buff);
                str = Encoding.UTF8.GetString(buff, 0, cnt);  //以上三行就是把接收到的数据转成正常的格式存到str里面
                Console.WriteLine("--------------------------------------------------");
                Console.WriteLine("从客户端获取的JSON串如下：");
                Console.WriteLine(str);
                Base To_server = new Base();  //这行和下面那行是用来判断具体传输的是什么东西，详情请看DotNet.Utilities
                Base From_server = SerializeHelper.FromJson<Base>(str);  //将str里面的内容取出来用
                switch (From_server.Id)  //判断是什么操作
                {
                    case "111":  //学生注册第一步
                        CTS_reg1 cts_reg1 = SerializeHelper.FromJson<CTS_reg1>(From_server.Data.ToString());  //传给服务端的类，几就是接收的类
                        STC_reg1 stc_reg1 = new STC_reg1();  //传给客户端的类
                        stc_reg1 = Query.Stc_reg1(cts_reg1.s_Id,cts_reg1.s_Name);  //调用函数来完成那个传给客户端类的内容的填充
                        Console.WriteLine("当前为学生注册第一步，注册学号为：{0}", cts_reg1.s_Id);
                        Console.WriteLine();
                        if (stc_reg1.Result == "2")
                            To_server.Id = "-1013";
                        else
                            if (stc_reg1.Result == "0")
                            To_server.Id = "-1012";
                        else
                            if (stc_reg1.Result == "3")
                            To_server.Id = "-1011";
                        else
                            To_server.Id = "1";
                        To_server.Data = stc_reg1;
                        str = SerializeHelper.ToJson(To_server);  //把类变成json字符串
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);  //以上是发送的代码
                        break;
                    case "113":  //学生注册第二步
                        CTS_reg2 cts_reg2 = SerializeHelper.FromJson<CTS_reg2>(From_server.Data.ToString());
                        STC_reg2 stc_reg2 = new STC_reg2();
                        stc_reg2 = Query.Stc_reg2(cts_reg2.s_Id);
                        Console.WriteLine("当前为学生注册第二步，注册学号为：{0}", cts_reg2.s_Id);
                        Console.WriteLine();
                        if(stc_reg2.Result=="0")
                            To_server.Id = "-1031";
                        else
                            To_server.Id = "1";
                        To_server.Data = stc_reg2;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "114":  //学生注册第三步
                        CTS_reg3 cts_reg3 = SerializeHelper.FromJson<CTS_reg3>(From_server.Data.ToString());
                        Query.reg3(cts_reg3.s_Id, cts_reg3.s_Password);
                        Console.WriteLine("当前为学生注册第三步，注册学号为：{0}", cts_reg3.s_Id);
                        Console.WriteLine();
                        str = "完成注册";
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "211":  //学生请求登录
                        CTS_login cts_login = SerializeHelper.FromJson<CTS_login>(From_server.Data.ToString());
                        STC_login stc_login = new STC_login();
                        stc_login = Query.Stc_login(cts_login.s_Id, cts_login.s_Password);
                        Console.WriteLine("当前为学生请求登录，登录学号为：{0}", cts_login.s_Id);
                        Console.WriteLine();
                        if(stc_login.Result=="0")
                            To_server.Id = "-2011";
                        else
                            To_server.Id = "1";
                        To_server.Data = stc_login;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "311":  //学生请求刷新
                        CTS_Refresh cts_refresh= SerializeHelper.FromJson<CTS_Refresh>(From_server.Data.ToString());
                        STC_Refresh stc_refresh = new STC_Refresh();
                        stc_refresh = Query.Stc_refresh(cts_refresh.s_Class);
                        Console.WriteLine("当前为学生请求刷新，该学生的班级为：{0}", cts_refresh.s_Class);
                        Console.WriteLine();
                        if (stc_refresh.Result == "0")
                            To_server.Id = "-3011";
                        else
                            To_server.Id = "1";
                        To_server.Data = stc_refresh;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "412":  //学生请求签到
                        CTS_Attend cts_attend = SerializeHelper.FromJson<CTS_Attend>(From_server.Data.ToString());
                        STC_Attend stc_attend = new STC_Attend();
                        stc_attend = Query.Stc_attend(cts_attend.s_Id, cts_attend.c_Id, cts_attend.t_Id, cts_attend.s_Class, cts_attend.s_Name,cts_attend.f_Id);
                        Console.WriteLine("当前为学生请求签到，签到学号为：{0}", cts_attend.s_Id);
                        Console.WriteLine();
                        if (stc_attend.Result=="0")
                            To_server.Id = "-4021";
                        else
                            To_server.Id = "1";
                        To_server.Data = stc_attend;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "511":  //老师请求登录
                        CTS_TLogin cts_tlogin = SerializeHelper.FromJson<CTS_TLogin>(From_server.Data.ToString());
                        STC_TLogin stc_tlogin = new STC_TLogin();
                        stc_tlogin = Query.Stc_tlogin(cts_tlogin.t_Id, cts_tlogin.t_Password);
                        Console.WriteLine("当前为老师请求登录，登录工号为：{0}", cts_tlogin.t_Id);
                        Console.WriteLine();
                        if (stc_tlogin.Result == "0")
                            To_server.Id = "-5011";
                        else
                            To_server.Id = "1";
                        To_server.Data = stc_tlogin;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "512":  //老师请求修改密码
                        CTS_TChange cts_tchange = SerializeHelper.FromJson<CTS_TChange>(From_server.Data.ToString());
                        STC_TChange stc_tchange = new STC_TChange();
                        stc_tchange = Query.Stc_tchange(cts_tchange.t_Id, cts_tchange.t_Password);
                        Console.WriteLine("当前为老师请求修改密码，老师工号为：{0}", cts_tchange.t_Id);
                        Console.WriteLine();
                        To_server.Id = "1";
                        To_server.Data = stc_tchange;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "611":  //老师请求添加课程信息
                        CTS_Add cts_add = SerializeHelper.FromJson<CTS_Add>(From_server.Data.ToString());
                        STC_Add stc_add = new STC_Add();
                        stc_add = Query.Stc_add(cts_add.t_Id, cts_add.s_Class, cts_add.c_Name, cts_add.c_startDay, cts_add.c_endDay, cts_add.c_Time, cts_add.c_Week, cts_add.c_Classroom);
                        Console.WriteLine("当前为老师请求添加课程信息，课程名称为：{0}",cts_add.c_Name);
                        Console.WriteLine();
                        To_server.Id = "1";
                        To_server.Data = stc_add;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "612":  //老师请求修改课程信息  未完成
                        CTS_Change cts_change = SerializeHelper.FromJson<CTS_Change>(From_server.Data.ToString());
                        STC_Change stc_change = new STC_Change();
                        //stc_change = Query.Stc_change();
                        Console.WriteLine("当前为老师请求修改课程信息，课程信息为：{0}", cts_change.s_Id);
                        Console.WriteLine();
                        To_server.Data = stc_change;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "613":  //老师请求刷新课程信息
                        CTS_TRefresh cts_trefresh = SerializeHelper.FromJson<CTS_TRefresh>(From_server.Data.ToString());
                        STC_TRefresh stc_trefresh = new STC_TRefresh();
                        stc_trefresh = Query.Stc_trefresh(cts_trefresh.t_Id);
                        Console.WriteLine("当前为老师请求刷新课程信息，课程信息为：{0}", cts_trefresh.t_Id);
                        Console.WriteLine();
                        To_server.Id = "1";
                        To_server.Data = stc_trefresh;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "711":  //老师发起签到
                        CTS_TAttend cts_tattend = SerializeHelper.FromJson<CTS_TAttend>(From_server.Data.ToString());
                        STC_TAttend stc_tattend = new STC_TAttend();
                        stc_tattend = Query.Stc_tattend(cts_tattend.t_Id, cts_tattend.c_Id,cts_tattend.t_Uuid);
                        Console.WriteLine("当前为老师请求发起签到，发起签到的老师工号为：{0}，课程ID为：{1}", cts_tattend.t_Id,cts_tattend.c_Id);
                        Console.WriteLine();
                        To_server.Id = "1";
                        To_server.Data = stc_tattend;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "712":  //老师结束签到
                        CTS_TSAttend cts_tsattend = SerializeHelper.FromJson<CTS_TSAttend>(From_server.Data.ToString());
                        STC_TSAttend stc_tsattend = new STC_TSAttend();
                        stc_tsattend = Query.Stc_tsattend(cts_tsattend.t_Id, cts_tsattend.c_Id);
                        Console.WriteLine("当前为老师请求结束签到，结束签到的老师工号为：{0}，课程ID为：{1}", cts_tsattend.t_Id, cts_tsattend.c_Id);
                        Console.WriteLine();
                        To_server.Id = "1";
                        To_server.Data = stc_tsattend;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "811":  //老师请求数据分析
                        CTS_Analysis cts_analysis = SerializeHelper.FromJson<CTS_Analysis>(From_server.Data.ToString());
                        STC_Analysis stc_analysis = new STC_Analysis();
                        stc_analysis = Query.Stc_analtsis(cts_analysis.c_Id);
                        Console.WriteLine("当前为老师请求数据分析，请求的课程ID为：{0}", cts_analysis.c_Id);
                        Console.WriteLine();
                        To_server.Id = "1";
                        To_server.Data = stc_analysis;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                    case "112": //学生在注册时获取签名
                    case "411": //学生在签到时获取签名
                        Signs signs = SerializeHelper.FromJson<Signs>(From_server.Data.ToString());
                        Signs true_signs = new Signs();
                        true_signs.Str = Sign.Signature(APP_ID, SECRET_ID, SECRET_KEY, DateTime.Now.ToUnixTime() / 1000 + 180, "", bucketName);
                        Console.WriteLine("当前为前台请求获取上传签名，得到的签名为：");
                        Console.WriteLine(true_signs.Str);
                        To_server.Id = "1";
                        To_server.Data = true_signs;
                        str = SerializeHelper.ToJson(To_server);
                        Console.WriteLine("发送给客户端JSON串如下：");
                        Console.WriteLine(str);
                        buff = new byte[65536];
                        buff = Encoding.UTF8.GetBytes(str);
                        mysocket.Send(buff);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                mysocket.Shutdown(SocketShutdown.Both);
                mysocket.Close();
            }
        }
    }
}
