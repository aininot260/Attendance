﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient; //这个是引用进来的用于操作MYSQL数据库的动态链接库
using DotNet.Utilities;  //这个是自己写的那个类库
using QCloud.PicApi.Api;  //这个是引用进来的人脸对比的API
using QCloud.PicApi.Common;
using System.Net;  //http通信的命名空间
using System.IO;
using System.Data;
using MySql.Data;
namespace DBOPlib
{
    public class DBOP
    {
        const int APP_ID = 1252885834;
        const string SECRET_ID = "AKIDfFgyABujEXecT8bt1xqX4QXTDl9LnWaT";
        const string SECRET_KEY = "qJhtfLsFWcZekfO1Shk7Og1HMFwRktwo";
        public static string connstr = @"server=47.94.193.149;uid=db_user;pwd=1234567890Bb;database=attendance;charset=utf8;port=3306";
        //数据库连接字符串
        MySqlConnection Conn = new MySqlConnection(connstr);
        MySqlConnection Conn1 = new MySqlConnection(connstr);
        //连接对象的初始化
        private static string HttpPost(string Url, string postDataStr)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = postDataStr.Length;
            StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.ASCII);
            writer.Write(postDataStr);
            writer.Flush();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string encoding = response.ContentEncoding;
            if (encoding == null || encoding.Length < 1)
            {
                encoding = "UTF-8"; //默认编码  
            }
            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(encoding));
            string retString = reader.ReadToEnd();
            return retString;
        }  //进行http通信的函数
//以下函数均返回对象类型，在函数内部进行数据库操作等
        public STC_reg1 Stc_reg1(string s_Id,string s_Name)  //判断学号
        {
            STC_reg1 stc_reg1 = new STC_reg1();
            string sql1 = "Select * From Student Where s_Id='" + s_Id + "'";
            string sql2 = "Select * From Student Where s_Id='" + s_Id + "' And s_Name='" + s_Name + "'";
            try
            {
                MySqlCommand com1 = new MySqlCommand(sql1, Conn);
                Conn.Open();
                MySqlDataReader dr1 = com1.ExecuteReader();
                dr1.Read();
                if (dr1.HasRows)
                {
                    string tmp = dr1["s_Password"].ToString();
                    dr1.Close();
                    MySqlCommand com2 = new MySqlCommand(sql2, Conn);
                    MySqlDataReader dr2 = com2.ExecuteReader();
                    dr2.Read();
                    if (dr2.HasRows)
                    {
                        if ( tmp== "")
                            stc_reg1.Result = "1";
                        else
                            stc_reg1.Result = "2";
                    }
                    else
                        stc_reg1.Result = "3";
                    dr2.Close();
                }
                else
                    stc_reg1.Result = "0";
            }
            finally
            {
                Conn.Close();
            }
            return stc_reg1;
        }
        public STC_reg2 Stc_reg2(string s_Id)  //判断是不是同一个人
        {
            STC_reg2 stc_reg2 = new STC_reg2();
            var result = "";
            var bucketName = "attendance";
            var pic = new PicCloud(APP_ID, SECRET_ID, SECRET_KEY);
            string t0 = "http://attendance-1252885834.cossh.myqcloud.com/";
            string t1 = s_Id[0].ToString() + s_Id[1].ToString();
            string t2 = s_Id[4].ToString() + s_Id[5].ToString() + s_Id[6].ToString() + s_Id[7].ToString();
            string url1 = t0 + t1 + "/" + t2 + "/" + s_Id + ".jpg";
            string url2 = t0 + t1 + "/" + "t_" + t2 + "/" + s_Id + ".jpg";
            string[] pornUrl = {url1,url2};
            result = pic.DetectionUrl(bucketName, pornUrl);
            float confindence = SerializeHelper.FromJson<TencentAPI>(result.ToString()).data.similarity;
            Console.WriteLine("以下内容为人脸比对结果：");
            Console.WriteLine(result);
            result = pic.Delete(bucketName, "/" + t1 + "/" + "t_" + t2 + "/" + s_Id + ".jpg");
            Console.WriteLine("以下内容为COS的处理结果：");
            Console.WriteLine("删除文件:" + result);
            if (confindence >= 60)
                stc_reg2.Result = "1";
            else
                stc_reg2.Result = "0";
            return stc_reg2;
        }
        //像这种纯操纵数据库的函数就不会返回类型了
        public void reg3(string s_Id, string s_Password)  //完成注册
        {
            string sql = "Update Student Set s_Password='" + s_Password + "' Where s_Id='" + s_Id + "'";
            try
            {
                MySqlCommand com = new MySqlCommand(sql, Conn);
                Conn.Open();
                com.ExecuteReader();
            }
            finally
            {
                Conn.Close();
            }
        }
        public STC_login Stc_login(string s_Id, string s_Password)  //学生登录
        {
            STC_login stc_login = new STC_login();
            string sql = "Select * From Student Where s_Id='" + s_Id + "'And s_Password='" + s_Password + "'";
            try
            {
                MySqlCommand com = new MySqlCommand(sql, Conn);
                MySqlCommand com3;
                Conn.Open();
                MySqlDataReader dr = com.ExecuteReader();
                MySqlDataReader dr3;
                dr.Read();
                if (dr.HasRows)
                {
                    stc_login.Result = "1";
                    stc_login.s_Name = dr["s_Name"].ToString();
                    stc_login.s_Class = dr["s_Class"].ToString();
                    dr.Close();
                    string sql3 = "Select * From Class Where s_Class='" + stc_login.s_Class + "'";
                    com3 = new MySqlCommand(sql3, Conn);
                    dr3 = com3.ExecuteReader();
                    dr3.Read();
                    stc_login.class_Name = dr3["class_Name"].ToString();
                    dr3.Close();
                }
                else
                {
                    stc_login.Result = "0";
                    dr.Close();
                }
            }
            finally
            {
                Conn.Close();
            }
            return stc_login;
        }
        public STC_Refresh Stc_refresh(string s_Class)  //学生刷新课程信息
        {
            STC_Refresh stc_refresh = new STC_Refresh();
            bool flag = false;
            string t_Id = "";
            string sql1 = "Select * From Course Where s_Class='" + s_Class + "'";
            try
            {
                MySqlCommand com1 = new MySqlCommand(sql1, Conn);
                MySqlCommand com2;
                MySqlCommand com3;
                Conn.Open();
                MySqlDataReader dr1 = com1.ExecuteReader();
                MySqlDataReader dr2;
                MySqlDataReader dr3;
                while (dr1.Read())
                {
                    if (dr1["Permission"].ToString() == "1")
                    {
                        stc_refresh.Result = "1";
                        stc_refresh.c_Name = dr1["c_Name"].ToString();
                        stc_refresh.c_Id = dr1["c_Id"].ToString();
                        stc_refresh.c_Classroom = dr1["c_Classroom"].ToString();
                        stc_refresh.c_Week = dr1["c_Week"].ToString();
                        stc_refresh.c_Time = dr1["c_Time"].ToString();
                        stc_refresh.t_Id = dr1["t_Id"].ToString();
                        t_Id = stc_refresh.t_Id;
                        dr1.Close();
                        string sql2 = "Select * From Teacher Where t_Id='" + t_Id + "'";
                        com2 = new MySqlCommand(sql2, Conn);
                        dr2 = com2.ExecuteReader();
                        dr2.Read();
                        stc_refresh.t_Name = dr2["t_Name"].ToString();
                        stc_refresh.t_Uuid = dr2["t_Uuid"].ToString();
                        dr2.Close();
                        string sql3 = "Select * From f_Time Where t_Id='" + t_Id + "' And c_Id='" + stc_refresh.c_Id + "'";
                        com3 = new MySqlCommand(sql3, Conn);
                        dr3 = com3.ExecuteReader();
                        while(dr3.Read())
                        stc_refresh.f_Id = dr3["Id"].ToString();
                        dr3.Close();
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    dr1.Close();
                    stc_refresh.Result = "0";
                }
            }
            finally
            {
                Conn.Close();
            }
            return stc_refresh;
        }
        public STC_Attend Stc_attend(string s_Id,string c_Id, string t_Id, string s_Class,string s_Name,string ff_Id)  //学生签到
        {
            Console.WriteLine("进入函数");
            STC_Attend stc_attend = new STC_Attend();
            string f_Time = "";
            string f_Id = "";
            bool flag = false;
            string sql = "Select * From Course Where s_Class='" + s_Class + "'";
            try
            {
                Console.WriteLine("进入try块儿");
                MySqlCommand com = new MySqlCommand(sql, Conn);
                Conn.Open();
                MySqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                    if(dr["Permission"].ToString() == "1")
                    {
                        flag = true;
                        break;
                    }
                dr.Close();
            }
            finally
            {
                Conn.Close();
            }
            Console.WriteLine("进入判断");
            if (flag)
            {
                var result = "";
                var bucketName = "attendance";
                Console.WriteLine("创建对象");
                var pic = new PicCloud(APP_ID, SECRET_ID, SECRET_KEY);
                string t0 = "http://attendance-1252885834.cossh.myqcloud.com/";
                string t1 = s_Id[0].ToString() + s_Id[1].ToString();
                string t2 = s_Id[4].ToString() + s_Id[5].ToString() + s_Id[6].ToString() + s_Id[7].ToString();
                string url1 = t0 + t1 + "/" + t2 + "/" + s_Id + ".jpg";
                string url2 = t0 + t1 + "/" + "t_" + t2 + "/" + s_Id +ff_Id+ ".jpg";
                string[] pornUrl = { url1, url2 };
                Console.WriteLine("拼接完成");
                result = pic.DetectionUrl(bucketName, pornUrl);
                Console.WriteLine("调用删除");
                float confindence = SerializeHelper.FromJson<TencentAPI>(result.ToString()).data.similarity;
                Console.WriteLine("解析对象");
                Console.WriteLine("以下内容为人脸比对结果：");
                Console.WriteLine(result);
                result = "";
                result = pic.Delete(bucketName, "/" + t1 + "/" + "t_" + t2 + "/" + s_Id + ff_Id + ".jpg");
                Console.WriteLine("以下内容为COS的处理结果：");
                Console.WriteLine("删除文件:" + result);
                if (confindence >= 60)
                {
                    stc_attend.Result = "1";
                    string sql1 = "Select * From f_Time Where t_Id='" + t_Id + "' And c_Id='" + c_Id + "'";
                    try
                    {
                        MySqlCommand com1 = new MySqlCommand(sql1, Conn);
                        Conn.Open();
                        MySqlDataReader dr = com1.ExecuteReader();
                        while(dr.Read())
                        {
                            f_Time = dr["f_Time"].ToString();
                            f_Id = dr["Id"].ToString();

                        }

                        dr.Close();
                    }
                    finally
                    {
                        Conn.Close();
                    }
                    string sql2 = "Insert Into Sign (c_Id,s_Id,s_Name,s_Class,f_Time,Date,q_Time,t_Id,f_Id) Values ('" + c_Id + "','" + s_Id + "','" + s_Name + "','" + s_Class + "','" + f_Time + "',CURDATE(),CURTIME(),'" + t_Id + "','" + f_Id + "')";
                    try
                    {
                        MySqlCommand com2 = new MySqlCommand(sql2, Conn);
                        Conn.Open();
                        com2.ExecuteNonQuery();
                    }
                    finally
                    {
                        Conn.Close();
                    }
                }
                else
                    stc_attend.Result = "0";
            }
            else
                stc_attend.Result = "0";
            return stc_attend;
        }
        public STC_TLogin Stc_tlogin(string t_Id, string t_Password)  //教师登录
        {
            STC_TLogin stc_tlogin = new STC_TLogin();
            string sql = "Select * From Teacher Where t_Id='" + t_Id + "' And t_Password='" + t_Password + "'";
            try
            {
                MySqlCommand com = new MySqlCommand(sql, Conn);
                Conn.Open();
                MySqlDataReader dr = com.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    stc_tlogin.Result = "1";
                    stc_tlogin.t_Name = dr["t_Name"].ToString();
                    stc_tlogin.t_Uuid = dr["t_Uuid"].ToString();
                }
                else
                    stc_tlogin.Result = "0";
                dr.Close();
            }
            finally
            {
                Conn.Close();
            }
            return stc_tlogin;
        }
        public STC_TChange Stc_tchange(string t_Id, string t_Password)  //教师修改密码
        {
            STC_TChange stc_tchange = new STC_TChange();
            string sql = "Update Teacher Set t_Password='" + t_Password + "' Where t_Id='" + t_Id + "'";
            try
            {
                MySqlCommand com = new MySqlCommand(sql, Conn);
                Conn.Open();
                com.ExecuteReader();
            }
            finally
            {
                Conn.Close();
            }
            stc_tchange.Result = "1";
            return stc_tchange;
        }
        public STC_Add Stc_add(string t_Id, List<string> s_class, string c_Name,string c_startDay,string c_endDay,string c_Time,string c_Week,string c_Classroom)  //教师添加课程信息
        {
            STC_Add stc_add = new STC_Add();
            string c_Id = "";
            string sql1 = "Select * From Course";
            try
            {
                MySqlCommand com = new MySqlCommand(sql1, Conn);
                Conn.Open();
                MySqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    c_Id = dr["c_Id"].ToString();
                }

                dr.Close();
            }
            finally
            {
                Conn.Close();
            }
            int tmp = Convert.ToInt32(c_Id);
            tmp++;
            c_Id = tmp.ToString();
            foreach (string item in s_class)
            {
                string sql = "Insert Into Course (c_Id,t_Id,s_Class,c_Name,c_startDay,c_endDay,c_Time,c_Week,c_Classroom) Values ('" + c_Id + "','" + t_Id + "','" + item + "','" + c_Name + "','" + c_startDay + "','" + c_endDay + "','" + c_Time + "','" + c_Week + "','" + c_Classroom + "')";
                try
                {
                    MySqlCommand com = new MySqlCommand(sql, Conn);
                    Conn.Open();
                    com.ExecuteNonQuery();
                }
                finally
                {
                    Conn.Close();
                }
            }
            stc_add.Result = "1";
            return stc_add;
        }
        public STC_TRefresh Stc_trefresh(string t_Id)  //教师刷新课程信息
        {
            STC_TRefresh stc_trefresh = new STC_TRefresh();
            Course_List course_list = new Course_List();
            List<Course> courselist=new List<Course>();
        string sql = "Select * From Course Where t_Id='" + t_Id + "'";
            try
            {
                MySqlCommand com = new MySqlCommand(sql, Conn);
                Conn.Open();
                MySqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    Course tmp = new Course();
                    tmp.c_Classroom= dr["c_Classroom"].ToString();
                    tmp.c_endDay = dr["c_endDay"].ToString();
                    tmp.c_Id = dr["c_Id"].ToString();
                    tmp.c_Name = dr["c_Name"].ToString();
                    tmp.c_startDay= dr["c_startDay"].ToString();
                    tmp.c_Time= dr["c_Time"].ToString();
                    tmp.c_Week= dr["c_Week"].ToString();
                    courselist.Add(tmp);
                }
                dr.Close();
            }
            finally
            {
                Conn.Close();
            }
            course_list.CourseList = courselist;
            stc_trefresh.classnumber = courselist.Count;
            stc_trefresh.list = course_list;
            return stc_trefresh;
        }
        public STC_TAttend Stc_tattend(string t_Id,string c_Id,string t_Uuid)  //教师发起签到
        {
            STC_TAttend stc_tattend = new STC_TAttend();
            string sql1 = "Insert Into f_Time (t_Id,c_Id,f_Time) Values ('" + t_Id + "','" + c_Id + "',CURTIME())";
            try
            {
                MySqlCommand com1 = new MySqlCommand(sql1, Conn);
                Conn.Open();
                com1.ExecuteReader();
            }
            finally
            {
                Conn.Close();
            }
            string sql2 = "Update Course Set Permission='" + "1" + "' Where t_Id='" + t_Id + "' And c_Id='" + c_Id + "'";
            try
            {
                MySqlCommand com2 = new MySqlCommand(sql2, Conn);
                Conn.Open();
                com2.ExecuteReader();
            }
            finally
            {
                Conn.Close();
            }
            string sql3 = "Update Teacher Set t_Uuid='" + t_Uuid + "' Where t_Id='" + t_Id + "'";
            try
            {
                MySqlCommand com3 = new MySqlCommand(sql3, Conn);
                Conn.Open();
                com3.ExecuteReader();
            }
            finally
            {
                Conn.Close();
            }
            stc_tattend.Result = "1";
            return stc_tattend;
        }
        public STC_TSAttend Stc_tsattend(string t_Id, string c_Id)  //教师结束签到
        {
            STC_TSAttend stc_tsattend = new STC_TSAttend();
            string s_Class="";
            string f_Id = "";
            string sql = "Update Course Set Permission='" + "0" + "' Where t_Id='" + t_Id + "' And c_Id='" + c_Id + "'";
            try
            {
                MySqlCommand com = new MySqlCommand(sql, Conn);
                Conn.Open();
                com.ExecuteReader();
            }
            finally
            {
                Conn.Close();
            }
            string f_Time = "";
            string sql2 = "Select * From f_Time Where t_Id='" + t_Id + "' And c_Id='" + c_Id + "'";
            try
            {
                MySqlCommand com2 = new MySqlCommand(sql2, Conn);
                Conn.Open();
                MySqlDataReader dr = com2.ExecuteReader();
                while(dr.Read())
                    f_Time = dr["f_Time"].ToString();
                dr.Close();
            }
            finally
            {
                Conn.Close();
            }
            stc_tsattend.Result = "1";
            Student_List student_list1 = new Student_List();  //签到学生类
            List<Student> studentlist1 = new List<Student>();  //签到学生数组
            Student_List student_list2 = new Student_List();  //迟到学生类
            List<Student> studentlist2 = new List<Student>();  //迟到学生数组
            string sql3 = "Select * From Sign Where t_Id='" + t_Id + "' And  c_Id='" + c_Id + "' And f_Time='" + f_Time + "' And Date = CURDATE()";
            try
            {
                MySqlCommand com3 = new MySqlCommand(sql3, Conn);
                Conn.Open();
                MySqlDataReader dr = com3.ExecuteReader();
                while (dr.Read())
                {
                    Student tmp = new Student();
                    tmp.s_Id = dr["s_Id"].ToString();
                    tmp.s_Name = dr["s_Name"].ToString();
                    tmp.q_Time = dr["q_Time"].ToString();
                    f_Id= dr["f_Id"].ToString();
                    studentlist1.Add(tmp);
                }
                dr.Close();
            }
            finally
            {
                Conn.Close();
            }
            string sql4 = "Select * From Course Where c_Id='" + c_Id + "'";
            try
            {
                MySqlCommand com4 = new MySqlCommand(sql4, Conn);
                Conn.Open();
                MySqlDataReader dr1 = com4.ExecuteReader();
                while (dr1.Read())
                {
                    s_Class= dr1["s_Class"].ToString();
                    string sql5 = "Select * From Student Where s_Class='" + s_Class + "'";
                    try
                    {
                        MySqlCommand com5 = new MySqlCommand(sql5, Conn1);
                        Conn1.Open();
                        MySqlDataReader dr = com5.ExecuteReader();
                        while (dr.Read())
                        {
                            Student tmp = new Student();
                            tmp.s_Id = dr["s_Id"].ToString();
                            tmp.s_Name = dr["s_Name"].ToString();
                            bool flag = false;
                            foreach (Student item in studentlist1)
                            {
                                if (item.s_Id == tmp.s_Id)
                                {
                                    flag = true;
                                    break;
                                }
                            }
                            if(!flag)
                                studentlist2.Add(tmp);
                        }
                        dr.Close();
                    }
                    finally
                    {
                        Conn1.Close();
                    }
                }
                dr1.Close();
            }
            finally
            {
                Conn.Close();
            }
            student_list1.StudentList = studentlist1;
            student_list2.StudentList = studentlist2;
            stc_tsattend.classnumber1 = studentlist1.Count;
            stc_tsattend.classnumber2 = studentlist2.Count;
            stc_tsattend.list1 = student_list1;
            stc_tsattend.list2 = student_list2;
            double a = Convert.ToDouble(studentlist1.Count) / Convert.ToDouble(studentlist1.Count + studentlist2.Count);
            double b = Convert.ToDouble(studentlist2.Count) / Convert.ToDouble(studentlist1.Count + studentlist2.Count);
            string attend_Ratio = a.ToString();
            string late_Ratio = "0.0";
            string absent_Ratio= b.ToString();
            string sql6 = "Insert Into Analysis (t_Id,c_Id,s_Class,Date,attend_Ratio,late_Ratio,absent_Ratio,f_Id) Values ('" + t_Id + "','" + c_Id + "','" + s_Class + "',CURDATE(),'" + attend_Ratio + "','" + late_Ratio + "','" + absent_Ratio + "','" + f_Id + "')";
            try
            {
                MySqlCommand com = new MySqlCommand(sql6, Conn);
                Conn.Open();
                com.ExecuteReader();
            }
            finally
            {
                Conn.Close();
            }
            return stc_tsattend;
        }
        public STC_Analysis Stc_analtsis(string c_Id)  //老师请求数据分析
        {
            STC_Analysis stc_analysis = new STC_Analysis();
            Analysis_List analysis_list = new Analysis_List();  //签到学生类
            List<Analysis> analysislist = new List<Analysis>();  //签到学生数组
            string sql = "Select * From Analysis Where c_Id='" + c_Id + "'";
            try
            {
                MySqlCommand com = new MySqlCommand(sql, Conn);
                Conn.Open();
                MySqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    Analysis tmp = new Analysis();
                    tmp.absent_Ratio = dr["absent_Ratio"].ToString();
                    tmp.attend_Ratio = dr["attend_Ratio"].ToString();
                    tmp.Date = dr["Date"].ToString();
                    tmp.f_Id = dr["f_Id"].ToString();
                    tmp.late_Ratio = dr["late_Ratio"].ToString();
                    tmp.s_Class = dr["s_Class"].ToString();
                    tmp.t_Id = dr["t_Id"].ToString();
                    analysislist.Add(tmp);
                }
                dr.Close();
            }
            finally
            {
                Conn.Close();
            }
            stc_analysis.Result = "1";
            analysis_list.AnalysisList = analysislist;
            stc_analysis.classnumber = analysislist.Count;
            stc_analysis.list = analysis_list;
            return stc_analysis;
        }
    }
}
