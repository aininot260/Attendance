﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;  //用于序列化的命名空间
using Newtonsoft.Json;  //json动态链接库


namespace DotNet.Utilities
{
    public class SerializeHelper
    {
        //以下是转换成json字符串和从json解码到类对象的两个函数
        public static string ToJson(object obj)
        {
            return  JsonConvert.SerializeObject(obj);
        }
        public static T FromJson<T>(string str) where T : class
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
    //以下的类均是用于数据传输的类，注意，Base用于判断信息类型，CTS是客户端发给服务端的类，STC是服务端发给客户端的类
    //有的类有多层嵌套比较复杂，建议好好捋一捋
    [DataContract]
    public class Base
    {
        [DataMember]
        public string Id
        {
            set;
            get;
        }
        [DataMember]
        public object Data
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_reg1
    {
        [DataMember]
        public string s_Name
        {
            set;
            get;
        }
        [DataMember]
        public string s_Id
        {
            set;
            get;
        }
    }  //111
    [DataContract]
    public class STC_reg1
    {
        [DataMember]
        public string Result  //取值为2：已被注册 取值为1：可以注册 取值为0：不存在
        {
            set;
            get;
        }
    }  
    [DataContract]
    public class CTS_reg2
    {
        [DataMember]
        public string s_Id
        {
            set;
            get;
        }
    }  //113
    [DataContract]
    public class STC_reg2
    {
        [DataMember]
        public string Result  //取值为1：是一个人 取值为0：不是一个人
        {
            set;
            get;
        }
    }  
    [DataContract]
    public class CTS_reg3
    {
        [DataMember]
        public string s_Id
        {
            set;
            get;
        }
        [DataMember]
        public string s_Password
        {
            set;
            get;
        }
    }  //114
    [DataContract]
    public class CTS_login
    {
        [DataMember]
        public string s_Id
        {
            set;
            get;
        }
        [DataMember]
        public string s_Password
        {
            set;
            get;
        }
    }  //211
    [DataContract]
    public class STC_login
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
        [DataMember]
        public string s_Name
        {
            set;
            get;
        }
        [DataMember]
        public string s_Class
        {
            set;
            get;
        }
        [DataMember]
        public string class_Name
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_Refresh
    {
        [DataMember]
        public string s_Class
        {
            set;
            get;
        }
    }  //311
    [DataContract]
    public class STC_Refresh
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
        [DataMember]
        public string t_Name
        {
            set;
            get;
        }
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
        [DataMember]
        public string c_Name
        {
            set;
            get;
        }
        [DataMember]
        public string c_Id
        {
            set;
            get;
        }
        [DataMember]
        public string c_Classroom
        {
            set;
            get;
        }
        [DataMember]
        public string c_Week
        {
            set;
            get;
        }
        [DataMember]
        public string c_Time
        {
            set;
            get;
        }
        [DataMember]
        public string t_Uuid
        {
            set;
            get;
        }
        [DataMember]
        public string f_Id
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_Attend
    {
        [DataMember]
        public string s_Id
        {
            set;
            get;
        }
        [DataMember]
        public string c_Id
        {
            set;
            get;
        }
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
        [DataMember]
        public string s_Class
        {
            set;
            get;
        }
        [DataMember]
        public string s_Name
        {
            set;
            get;
        }
        [DataMember]
        public string f_Id
        {
            set;
            get;
        }
    }  //412
    [DataContract]
    public class STC_Attend
    {
        [DataMember]
        public string Result  //取值为1：签到成功 取值为0：签到失败
        {
            set;
            get;
        }
    }  
    [DataContract]
    public class Signs
    {
        [DataMember]
        public string Str  //客户端请求时：内容为Asking 服务端传回来时为真实值
        {
            set;
            get;
        }
    }  //112 411
    [DataContract]
    public class CompareAPI
    {
        [DataMember]
        public string request_id
        {
            set;
            get;
        }
        [DataMember]
        public float confidence
        {
            set;
            get;
        }
        [DataMember]
        public object thresholds
        {
            set;
            get;
        }
        [DataMember]

        public string image_id1
        {
            set;
            get;
        }
        [DataMember]

        public string image_id2
        {
            set;
            get;
        }
        [DataMember]
        public int time_used
        {
            set;
            get;
        }
        [DataMember]
        public string error_message
        {
            set;
            get;
        }
    }  
    [DataContract]
    public class data
    {
        [DataMember]
        public string session_id
        {
            set;
            get;
        }
        [DataMember]
        public float similarity
        {
            set;
            get;
        }
        [DataMember]
        public int fail_flag
        {
            set;
            get;
        }
    }
    [DataContract]
    public class TencentAPI
    {
        [DataMember]
        public data data
        {
            set;
            get;
        }
        [DataMember]
        public int code
        {
            set;
            get;
        }
        [DataMember]
        public string message
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_TLogin
    {
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
        [DataMember]
        public string t_Password
        {
            set;
            get;
        }
    }  //511
    [DataContract]
    public class STC_TLogin
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
        [DataMember]
        public string t_Name
        {
            set;
            get;
        }
        [DataMember]
        public string t_Uuid
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_TChange
    {
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
        [DataMember]
        public string t_Password
        {
            set;
            get;
        }
    }  //512
    [DataContract]
    public class STC_TChange
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_Add
    {
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
        [DataMember]
        public List<string> s_Class
        {
            set;
            get;
        }
        [DataMember]
        public string c_Name
        {
            set;
            get;
        }
        [DataMember]
        public string c_startDay
        {
            set;
            get;
        }
        [DataMember]
        public string c_endDay
        {
            set;
            get;
        }
        [DataMember]
        public string c_Time
        {
            set;
            get;
        }
        [DataMember]
        public string c_Week
        {
            set;
            get;
        }
        [DataMember]
        public string c_Classroom
        {
            set;
            get;
        }
    }  //611
    [DataContract]
    public class STC_Add
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_Change
    {
        [DataMember]
        public string s_Id
        {
            set;
            get;
        }
    }  //612
    [DataContract]
    public class STC_Change
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
    }
    [DataContract]
    public class Course

    {
        [DataMember]
        public string c_Id
        {
            set;
            get;
        }
        [DataMember]
        public string c_Name
        {
            set;
            get;
        }
        [DataMember]
        public string c_startDay
        {
            set;
            get;
        }
        [DataMember]
        public string c_endDay
        {
            set;
            get;
        }
        [DataMember]
        public string c_Time
        {
            set;
            get;
        }
        [DataMember]
        public string c_Week
        {
            set;
            get;
        }
        [DataMember]
        public string c_Classroom
        {
            set;
            get;
        }
    }
    [DataContract]
    public class Course_List
    {
        [DataMember]
        public List<Course> CourseList
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_TRefresh
    {
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
    }  //613
    [DataContract]
    public class STC_TRefresh
    {
        [DataMember]
        public int classnumber
        {
            set;
            get;
        }
        [DataMember]
        public Course_List list
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_TAttend
    {
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
        [DataMember]
        public string c_Id
        {
            set;
            get;
        }
        [DataMember]
        public string t_Uuid
        {
            set;
            get;
        }
    }  //711
    [DataContract]
    public class STC_TAttend
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
    }
    [DataContract]
    public class Student

    {
        [DataMember]
        public string s_Id
        {
            set;
            get;
        }
        [DataMember]
        public string s_Name
        {
            set;
            get;
        }
        [DataMember]
        public string q_Time
        {
            set;
            get;
        }
    }
    [DataContract]
    public class Student_List
    {
        [DataMember]
        public List<Student> StudentList
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_TSAttend
    {
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
        [DataMember]
        public string c_Id
        {
            set;
            get;
        }
    }  //712
    [DataContract]
    public class STC_TSAttend
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
        [DataMember]
        public int classnumber1
        {
            set;
            get;
        }
        [DataMember]
        public Student_List list1
        {
            set;
            get;
        }
        [DataMember]
        public int classnumber2
        {
            set;
            get;
        }
        [DataMember]
        public Student_List list2
        {
            set;
            get;
        }
    }
    [DataContract]
    public class Analysis
    {
        [DataMember]
        public string t_Id
        {
            set;
            get;
        }
        [DataMember]
        public string s_Class
        {
            set;
            get;
        }
        [DataMember]
        public string Date
        {
            set;
            get;
        }
        [DataMember]
        public string attend_Ratio
        {
            set;
            get;
        }
        [DataMember]
        public string late_Ratio
        {
            set;
            get;
        }
        [DataMember]
        public string absent_Ratio
        {
            set;
            get;
        }
        [DataMember]
        public string f_Id
        {
            set;
            get;
        }
    }
    [DataContract]
    public class Analysis_List
    {
        [DataMember]
        public List<Analysis> AnalysisList
        {
            set;
            get;
        }
    }
    [DataContract]
    public class CTS_Analysis
    {
        [DataMember]
        public string c_Id
        {
            set;
            get;
        }
    }  //811
    [DataContract]
    public class STC_Analysis
    {
        [DataMember]
        public string Result
        {
            set;
            get;
        }
        [DataMember]
        public int classnumber
        {
            set;
            get;
        }
        [DataMember]
        public Analysis_List list
        {
            set;
            get;
        }
    }
}
